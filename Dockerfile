FROM ubuntu:23.04
LABEL Author="Zach Laster <zlaster@verifa.io>"

RUN apt-get update && apt-get install -y --no-install-recommends \
	ca-certificates \
	unzip \
	zip \
	curl \
	&& rm -rf /var/lib/apt/lists/*

ADD get_butler.sh /opt/butler/get_butler.sh
RUN bash /opt/butler/get_butler.sh
RUN /opt/butler/bin/butler -V

ENV PATH="/opt/butler/bin:${PATH}"
